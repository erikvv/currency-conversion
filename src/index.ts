
import * as http from 'http'
import * as express from "express"
import {Request, Response, NextFunction} from "express";
import { convertUsingDefaultBackendFailover } from './backends/default-failover';

const app = express()

app.use(express.static('public'))

app.route('/rate')
    .get((req: Request, res: Response, next: NextFunction) => {
        convertUsingDefaultBackendFailover(req.query)
            .subscribe(
                conversionResult => res.json(conversionResult),
                error => {
                    res.statusCode = 500
                    res.json(error)
                }
            )
    })

http.createServer(app).listen(3000, () => {
    console.log('Express started')
})
