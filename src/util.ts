import { OperatorFunction, empty } from "rxjs";
import { catchError } from "rxjs/operators";

export const ignoreError: OperatorFunction<any, any> = catchError(() => empty())
