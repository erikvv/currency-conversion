
import { fromEvent, merge, of } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { httpClient } from './http-client'
import { ignoreError } from '../util'

const form = document.forms[0]
const resultElement = document.getElementById('result')

merge(of(true), fromEvent(form, 'change'))
    .pipe(
        mergeMap(() => {
            resultElement.innerText = '';
            const formData = new FormData(form)

            return httpClient.get('/rate', {
                params: {
                    from: formData.get('from') as string,
                    to: formData.get('to')  as string,
                }
            }).pipe(ignoreError)
        })
    )
    .subscribe(response => resultElement.innerText = 'Rate: ' + response.rate)