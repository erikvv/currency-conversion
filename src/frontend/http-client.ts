
import {HttpClient, HttpXhrBackend, ɵangular_packages_common_http_http_d as BrowserXhr} from '@angular/common/http'

export const httpClient = new HttpClient(new HttpXhrBackend(new BrowserXhr()))
