import { ConversionFn } from "."
import { throwError } from "rxjs"
import { BackendInfo, BackendFailover } from "./failover"
import { convertUsingCurrencyConverterApiDotCom } from "./currency-converter-api"
import { Duration } from "js-joda"
import { convertUsingKowabunga } from "./kowabunga"

export const timeoutConversionFn: ConversionFn = () => throwError(new Error('None of the backends answered in time'))

export const defaultBackendPriorityList: BackendInfo[] = [
    {
        convert: convertUsingCurrencyConverterApiDotCom,
        timeout: Duration.ofMillis(400),
    }, {
        convert: convertUsingKowabunga,
        timeout: Duration.ofMillis(5000),
    },  {
        convert: timeoutConversionFn,
    }
]

export const convertUsingDefaultBackendFailover: ConversionFn = new BackendFailover(defaultBackendPriorityList).convert

