
import { httpClient } from '../http-client'
import { ConversionFn, ConversionRequest, ConversionResult, BackendName } from '.'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

export const convertUsingKowabunga: ConversionFn = function(req: ConversionRequest): Observable<ConversionResult> {
    // api only works with a date, no time
    const today = new Date().toISOString().substr(0, 10)

    return httpClient.get('http://currencyconverter.kowabunga.net/converter.asmx/GetConversionRate', {
        responseType: 'text',
        params: {
            CurrencyFrom: req.from,
            CurrencyTo: req.to,
            RateDate: today, 
        }
    })
    .pipe(
        map(getRateFromXML),
        map(rate => ({
                from: req.from,
                to: req.to,
                rate: rate,
                timestamp: new Date(),
                backend: 'Kowabunga' as BackendName,
            })
        )
    )
}

function getRateFromXML(responseBody: string) {
    console.log(responseBody);
    const rate = +responseBody.match(/<decimal.*>(\d+\.\d+)<\/decimal>/)[1]

    if (Number.isNaN(rate)) {
        throw new Error('Unknown format or zero value from Kowabunga')
    }

    return rate
}