
import { ConversionFn, ConversionResult, ConversionRequest } from "."
import { Duration } from "js-joda"
import { Observable, timer, race } from "rxjs"
import { mergeMap } from "rxjs/operators"
import { boundMethod } from 'autobind-decorator'
import { ignoreError } from "../util";

export interface BackendInfo {
    convert: ConversionFn
    timeout?: Duration // time to wait before trying the next backend
}

/**
 * This failover class will try all supplied backends until one succeeds
 * 
 * If a timeout occurs on the first backend it will try the second one while keeping the first in flight, 
 * so the first backend can still fulfill the request even after its timeout has passed
 */
export class BackendFailover {
    constructor(
        private readonly backendPriorityList: BackendInfo[],
    ) {}

    @boundMethod
    convert(conversionRequest: ConversionRequest): Observable<ConversionResult> {
        const observables = this.backendPriorityList.map((backendInfo, i) => {
            const waitDuration = this.backendPriorityList.map(obj => obj.timeout)
                .slice(0, i)
                .reduce(
                    (acc, cur) => acc.plusDuration(cur ? cur : Duration.ZERO), 
                    Duration.ZERO
                )
            
            return timer(waitDuration.toMillis())
                .pipe(
                    // TODO: try the next request immediately after failure rather than waiting for the timeout
                    mergeMap(() => backendInfo.convert(conversionRequest).pipe(ignoreError))
                )
        })
    
        // race!
        return race(observables)
    }
}
