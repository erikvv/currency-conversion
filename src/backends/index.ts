
import {Observable} from 'rxjs'

export type Currency = 
    'EUR' | 
    'USD' | 
    'GBP'

export type BackendName = 
    'CurrencyConverterApiDotCom' | 
    'Kowabunga' |
    string

export interface ConversionRequest {
    from: Currency
    to: Currency
}

export interface ConversionResult {
    from: Currency
    to: Currency
    rate: number
    timestamp: Date
    backend: BackendName
}

export interface ConversionFn {
    (req: ConversionRequest): Observable<ConversionResult>
}
