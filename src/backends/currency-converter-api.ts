import { Observable } from 'rxjs'
import { ConversionFn, ConversionRequest, ConversionResult } from '.'
import { httpClient } from '../http-client'
import { map } from 'rxjs/operators'
import { get } from 'lodash'

export const convertUsingCurrencyConverterApiDotCom: ConversionFn = function(req: ConversionRequest): Observable<ConversionResult> {
    const concat = req.from + '_' + req.to;

    return httpClient.get('https://free.currencyconverterapi.com/api/v6/convert', {
        observe: 'body',
        responseType: 'json',
        params: {
            q: concat,
        },
    }).pipe(
        map(jsonResponse => {
            const result = get(jsonResponse, `results.${concat}`)
            if (!result) {
                throw new Error('Unknown response format from currencyconverterapi.com')
            }
            return {
                to: result.to,
                from: result.fr,
                rate: result.val,
                timestamp: new Date(),
                backend: 'CurrencyConverterApiDotCom',
            } as ConversionResult
        })
    )
}