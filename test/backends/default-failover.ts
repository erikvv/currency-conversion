
import { expect } from 'chai'
import { convertUsingDefaultBackendFailover } from '../../src/backends/default-failover';

describe('The default failover', () => {
    it('should give a reasonable USD => EUR conversion rate', done => {
        convertUsingDefaultBackendFailover({
            from: 'USD',
            to: 'EUR',
        }).subscribe(
            result => expect(result.rate).to.be.within(0.3, 2),
            error => done(error),
            () => done(),
        )
    })
})






