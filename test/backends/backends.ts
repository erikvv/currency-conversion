
import { expect } from 'chai'
import { Currency } from '../../src/backends'
import { usdEurConversionRequest } from './assets'
import { convertUsingKowabunga } from '../../src/backends/kowabunga'
import { convertUsingCurrencyConverterApiDotCom } from '../../src/backends/currency-converter-api'

const implementedBackends = [{
    name: 'Kowabunga',
    convert: convertUsingKowabunga
}, {
    name: 'CurrencyConverterApiDotCom',
    convert: convertUsingCurrencyConverterApiDotCom,
}]

for (const backend of implementedBackends) {
    describe(backend.name, () => {
        it('should have a reasonable USD => EUR conversion rate', done => {
            backend.convert(usdEurConversionRequest)
                .subscribe(
                    result => expect(result.rate).to.be.within(0.3, 2),
                    error => done(error),
                    () => done(),
                )
        })
    
        it('should error on unknown currency', done => {
            backend.convert({
                from: 'USD',
                to: 'BEERCAPS' as Currency, // subvert type system
            }).subscribe({
                error: () => done(),
            })
        })
    })
}
