import { BackendFailover } from '../../src/backends/failover';
import { convertUsingErroringBackend, convertUsingFastBackend, usdEurConversionRequest, convertUsingSlowBackend } from './assets';
import { expect } from 'chai'
import { Duration } from 'js-joda';

describe('The failover', () => {
    it("should give an answer from the first backend if it's the fastest", done => {
        const failover = new BackendFailover([
            {
                convert: convertUsingFastBackend,
                timeout: Duration.ofMillis(50),
            }, {
                convert: convertUsingSlowBackend,
            }
        ])

        failover.convert(usdEurConversionRequest)
            .subscribe(
                result => expect(result.backend).to.equal('FastBackend'),
                error => done(error),
                () => done()
            )
    })

    it('should go to the second backend in case of errors', done => {
        const failover = new BackendFailover([
            {
                convert: convertUsingErroringBackend,
                timeout: Duration.ofMillis(50),
            }, {
                convert: convertUsingSlowBackend,
            }
        ])

        failover.convert(usdEurConversionRequest)
            .subscribe(
                result => expect(result.backend).to.equal('SlowBackend'),
                error => done(error),
                () => done()
            )
    })

    it('should go to the second backend if the first one has timed out', done => {
        const failover = new BackendFailover([
            {
                convert: convertUsingSlowBackend,
                timeout: Duration.ofMillis(50),
            }, {
                convert: convertUsingFastBackend,
            }
        ])

        failover.convert(usdEurConversionRequest)
            .subscribe(
                result => expect(result.backend).to.equal('FastBackend'),
                error => done(error),
                () => done()
            )
    })
})

/* continues on next page */