import { ConversionRequest, ConversionResult, ConversionFn } from "../../src/backends";
import { timer, throwError, of } from "rxjs";
import { map } from "rxjs/operators";

export const usdEurConversionRequest: ConversionRequest = {
    from: 'USD',
    to: 'EUR',
}

const defaultResult: ConversionResult = {
    from: 'USD',
    to: 'EUR',
    rate: 0.9,
    timestamp: new Date(),
    backend: 'DefaultBackend',
}

export const convertUsingFastBackend: ConversionFn = () => of({
    ...defaultResult,
    backend: 'FastBackend',
})

// we are calling 100ms slow so the tests can run fast
export const convertUsingSlowBackend: ConversionFn = 
    (req: ConversionRequest) => timer(100).pipe(
        map(() => ({
                ...defaultResult,
                backend: 'SlowBackend',
            })
        )
    )

export const convertUsingErroringBackend: ConversionFn = () => throwError(new Error('Something is wrong with this backend'))
